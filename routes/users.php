<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\Notification;
use App\Http\Controllers\PDFController;


/*
|--------------------------------------------------------------------------
| API Routes
|-------------------------------------------------------------------------- 
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/',  [UserController::class, 'index']);
Route::get('/register',  [UserController::class, 'register']);
 
Route::post('/authenticate',  [UserController::class, 'authenticate'])->name('users');
Route::get('/dashboard',  [UserController::class, 'dashboard'])->name('users') ;
Route::get('/product/{id}',  [UserController::class, 'ViewUserProductDetails'])->name('users') ;
 Route::get('/register',  [UserController::class, 'register'])->name('users') ; 
Route::post('/getCitys',  [UserController::class, 'getCitys'])->name('users') ; 
Route::post('signUp',  [UserController::class, 'signUp'])->name('users.signUp') ; 
// Route::get('verify',  [UserController::class, 'mobileVerify'])->name('users') ; 
Route::post('postVerify',  [UserController::class, 'postVerify'])->name('users.postVerify') ; 
 


Route::get('/ticketCreate/{slug}',  [TicketController::class, 'index'])->name('users') ; 
Route::post('postTicket',  [TicketController::class, 'postTicket'])->name('users.postTicket') ; 



 Route::post('/logout',  [UserController::class, 'logout'])->name('users');
 
 Route::group(['prefix' => 'users'], function () {
   

   
 });
Route::post('markNotification',  [Notification::class, 'markNotification'])->name('users.markNotification');
Route::post('markRequestAll',  [Notification::class, 'markAllNotification'])->name('users.markRequestAll');
Route::get('/allNotification',  [Notification::class, 'displayNotification']); 
 
 //Pdf

Route::get('pdf/{id}', [PDFController::class, 'pdf'])->name('users');
 


 // Route::post('/logout',  [UserController::class, 'dashboard'])->name('users');
// Route::group(['prefix' => 'users'], function () {
 // Route::post('login',  [UserController::class, 'authenticate'])->name('customer.login');
//});
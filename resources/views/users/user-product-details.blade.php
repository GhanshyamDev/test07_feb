@extends('layouts.customer')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h2 class="m-0" id="headingPrint">  Products Detail</h2>
        </div><!-- /.col -->

        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"> <a  class="btn btn-primary btn-sm" href="#"><i class="fas fa-download" style="font-size: 15px;"></i> Terms & Condition</a> 
            </li> 
            <li class="breadcrumb-item"> <a  class="btn btn-primary btn-sm" href="#"><i class="fas fa-download" style="font-size: 15px;"></i>Invoice</a> 
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header --> 
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid"  id="pageDIv">

         <div class="row">
          <div class="col-12">
            <div class="card">

              <div class="card-body"  id="reportPrinting">
                 <div class="row invoice-info">
                  <div class="col-sm-4 invoice-col">
                    <h4>User  Details</h4>
                       <div class="template-demo">
                         <h4>Name. {{ session('Customer_logged')['name'] }}</h4>
                         <h4>Email. {{ session('Customer_logged')['email'] }}</h4>
                         <h4>Aadhar. XXXXXXXX</h4>
                         <h4>Mobile. {{ $userProductDetails['mobile'] }}</h4>
                        <h4>Location. {{ $city['city_name'] }},{{ $state['state_name'] }}, {{ $userProductDetails['pincode'] }}</h4>
                     </div>
                  </div> 

                  <!-- /.col -->
                  <div class="col-sm-4 invoice-col">
                    <h4>Product  Details</h4>
                       <div class="template-demo">
                          <h4>Name. {{ $userProductDetails['product_name'] }}</h4>
                          <h4>Price. {{ $userProductDetails['product_price'] }}</h4>
                          <h4>Brand. {{ $userProductDetails['category'] }}</h4>
                          <h4>warranty. {{ $userProductDetails['product_warranty'] }}</h4>
                       </div>
                       </div>
                  <!-- /.col -->
                  <div class="col-sm-4 invoice-col">
                    <div class="template-demo">
                          <h4>Purchase End. {{ date('d M Y', strtotime($userProductDetails['pauchase_date']))  }}</h4>
                          <h4>Warranty Date. {{ date('d M Y', strtotime($userProductDetails['warranty_start']))  }}</h4>
                          <h4>Warranty Start. {{ date('d M Y', strtotime($userProductDetails['warranty_end']))  }}</h4>
                         
                       </div>
                   </div>
                  <!-- /.col -->
                </div>
                <hr/>

                <div class="row no-print">
                  <div class="col-12">
                   
                    <a href="{{ url('users/pdf') }}/{{ $userProductDetails['product_id'] }}" type="button" class="btn btn-outline-primary-btn btn-icon-text"> Print <i class="mdi mdi-printer btn-icon-append"></i>
                    </a>
                   <!--   <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                  <i class="fas fa-download"></i> Generate PDF
                  </button> -->
                  </div>
                </div>
             </div>
            </div>
          </div>
       </div>


       <div class="row mt-3">
         <div class="col-lg-3 col-sm-6 col-md-5 grid-margin  ">
          <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title"> <img src="{{ asset('img/service.png')}}" style="width:40px;"></h4>
              <p class="card-description"><a class="text-primary f1  " href="tel:123-456-7890">Call Service Center</a></code></p>
             </div>
          </div>
        </div>
         <div class="col-lg-3 col-sm-6 col-md-5 grid-margin  ">
          <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title"> <img src="{{ asset('img/renew.webp')}}" style="width:40px;"></h4>
              <p class="card-description"><a href="{{ url('users/ticketCreate') }}/{{ Str::slug($userProductDetails['product_name'], '-') }}" class="text-primary f1  " href="#">Renew Service</a></code></p>
             </div>
          </div>
        </div>

        <div class="col-lg-3 col-sm-4 col-md-5 grid-margin  ">
          <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title"> <img src="{{ asset('img/ser-req.png')}}" style="width:40px;"></h4>
              <p class="card-description text-primary f1"> Create Service Request</p>
             </div>
          </div>
        </div> 

        <div class="col-lg-3 col-sm-4  col-md-5 grid-margin  ">
          <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title"> <img src="{{ asset('img/whatsapp.png')}}" style="width:40px;"></h4>
              <p class="card-description text-primary f1">WhatsApp</p>
             </div>
          </div>
        </div> 
      </div>
     </div> 
</section>
 
</div>
@endsection

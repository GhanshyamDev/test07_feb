@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mt-3">
        <div class="col-sm-6">
          <h2 class="m-0">   User Notification</h2>
        </div><!-- /.col -->

        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Notification</li>
          </ol>
        </div> 
      </div> 
    </div> 
  </div>


  <section class="content">

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card  ">
            @if(Session::has('success'))
            <div class="alert alert-success">
              <strong>Success!</strong> {{Session::get('success')}}
            </div>
            @elseif(Session::has('failed'))
            <div class="alert alert-success">
              <strong>Failed!</strong> {{Session::get('failed')}}
            </div>
            @endif
            <div class="card-body">

              <form method="post" action=" {{ route('userNotification') }}">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="col-md-6">
                     <div class="form-group">
                        <label class="small mb-1" for="inputFirstName">Manufacturer</label>
                          <select class="form-control " name="manufacturer" onchange="getStateCity()" style="width: 100%;border: 1px solid #c1baba;"  autofocus>
                            <option value=" ">Select Manufacturer</option>
                            <option value="ALL">Send All</option>
                            @if (count($Categories))
                            @foreach ($Categories as $cat)  
                            <option value="{{ $cat['category_id']}}">{{ $cat['category']}}</option>
                            @endforeach
                            @else
                            <option>State Not Found</option> 
                            @endif
                          </select>
                        @if ($errors->has('manufacturer'))
                        <span class="text-danger">{{ $errors->first('manufacturer') }}</span>
                        @endif
                      </div>
                  </div>
                  
                </div>
                <div class="form-row">
                  <div class="col-md-6">
                     <div class="form-group">
                      <label for="exampleInputPassword1">Message(<span style="color: #db5252;font-size:15px;">Max. 100 char required</span>)</label>
                      <textarea  name="description" maxlength="100" class="form-control textareaCount" style="border: 1px solid #c1baba;"  ></textarea>
                        @if ($errors->has('description'))
                          <span class="text-danger">{{ $errors->first('description') }}</span>
                          @endif
                    </div>
                    <script type="text/javascript">
                        $('textarea').maxlength({
                             alwaysShow: true,
                            threshold: 10,
                            warningClass: "label label-success",
                            limitReachedClass: "label label-danger",
                            separator: ' out of ',
                            preText: 'You write ',
                            postText: ' chars.',
                            validate: true
                        });
                  </script>
                  </div>
                   

                </div>
                  
                <div class="form-row">
                  <div class="col-md-2">
                    <input type="submit" class="btn btn-primary btn-block" name="submit" value="Send"> 
                  </div>
                </div>
                <div class="form-group mt-4 mb-0"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
       
    </div>
  </div>
</div>

@endsection

 @extends('layouts.app')
 

@section('content')
 <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-sm-6">
            <h2 class="m-0">View AMC Request </h2>
          
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
  <section class="content">
      <div class="container-fluid">
       <div class="card">
        <div class="card-body">
          <div class="table-responsive ">
                <table  class="table stripe row-border order-column example">
                    <thead>
                      <tr>
                          <th>Sr NO</th>
                          <th>UserName</th>
                          <th>Product Name</th>
                          <th>City</th>
                           <th>Warranty Status</th>  
                      </tr>
                  </thead>
                  <tbody>  
                    @foreach ($renewlit as $list)  
                       <tr style="height:50px;">
                  <td>{{ $list['renv_ser_id'] }}</td>
                  <td>{{ $list['name'] }}</td>
                  <td>{{ $list['product_name'] }}</td>
                  <td>{{ $list['city_name'] }}</td>
                   
                   <?php 
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $list['warranty_start']);
                      $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $list['warranty_end']);
                      $diff_in_days = $to->diffInDays($from);
                      if($diff_in_days == 1)
                        { ?> 
                     <td>{{ $diff_in_days  }} Days <span class="badge badge-warning">Expire Soon</span></td>
                      <?php  }  elseif($diff_in_days == 0) { ?>
                     <td>{{ $diff_in_days  }} Days <span class="badge badge-danger">Expire </span></td>
                       <?php  }  else{ ?>
                       <td>{{ $diff_in_days  }} Days <span class="badge badge-success">In_warranty</span></td>
                      <?php } ?>
                      <!--  <td class="project-actions text-right">
                          <a class="btn btn-primary btn-sm" href="#">
                              <i class="fas fa-folder">  </i>  View  </a>
                          <a class="btn btn-info btn-sm" href="#">
                              <i class="fas fa-pencil-alt"> </i> Edit </a>
                          <a class="btn btn-danger btn-sm" href="#">
                              <i class="fas fa-trash"> </i> Delete  </a>
                      </td>  -->
                 </tr> 
                        @endforeach
                                
                 </tbody>
                 
                </table>
                <!-- /.table -->
          </div>
        </div>
        <!-- /.row (main row) -->
       </div><!-- /.container-fluid -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
 
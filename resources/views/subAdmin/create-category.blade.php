@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mt-3">
        <div class="col-sm-6">
          <h2 class="m-0">Category</h2>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard </li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    @if(empty($categoryEdit))  
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
       
      <div class="row">
        <div class="col-md-12">
          <div class="card  ">
            @if(Session::has('success'))
            <div class="alert alert-success">
              <strong>Success!</strong> {{Session::get('success')}}
            </div>
            @elseif(Session::has('failed'))
            <div class="alert alert-success">
              <strong>Failed!</strong> {{Session::get('failed')}}
            </div>
            @endif
            <div class="card-body">

              <form method="post" action=" {{ route('oemAdmin.createCategory') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                
                    <div class="form-group">
                      <label >Category</label>
                      <input class="form-control" name="category" type="text" placeholder="Enter Category">
                      @if ($errors->has('category'))
                      <span class="text-danger">{{ $errors->first('category') }}</span>
                      @endif
                    </div>
                   <div class="form-group">
                      <label class="small mb-1" for="inputLastName">Category Logo</label>
                      <br/>
                      <input  name="logo" type="file" >
                      @if ($errors->has('logo'))
                      <span class="text-danger">{{ $errors->first('logo') }}</span>
                      @endif
                    </div>
                   
                <div class="form-row">
                  <div class="col-md-2">
                    <input type="submit" class="btn btn-primary btn-block" name="submit" value="Create"> 
                  </div>
                </div>
                <div class="form-group mt-4 mb-0"></div>
              </form>
            </div>
          </div>
        </div>
       </div>

        <div class="row">
         <div class="col-md-12">  
          <div class="card mt-3">
             <div class="card-body">
               
              <div class="table-responsive">
                <table class="table table-bordered dataTables example " id=" " width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Sr NO</th>
                      <th>Name</th>
                      <th>Logo</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if (count($categories)) 

                    @foreach ($categories as $info)  
                    <tr>
                      <td>{{ $info['category_id'] }}</td>
                      <td>{{ $info['category'] }}</td>
                      <td> <ul class="list-inline">
                        <li class="list-inline-item">
                          <img alt="Avatar" class="table-avatar" style="width: 40px;" src="{{ asset('img/category/'.$info['logo'] ) }}">
                        </li>  </ul>
                      </td>
                      <td>
                        <a href="{{ url('oemAdmin/categoryEdit') }}/{{ Crypt::encryptString($info['category_id']) }}" class="btn btn-primary btn-sm"><i class="mdi mdi-grease-pencil"></i></a>   
                        <a href="{{ url('oemAdmin/categoryDelete') }}/{{ Crypt::encryptString($info['category_id']) }}" class="btn btn-primary btn-sm"><i class="mdi mdi-delete-forever"></i></i></a>            
                      </td>
                    </tr> 
                    @endforeach
                    @else
                    <tr> User  Not Found</tr> 
                    @endif 
                  </tbody>
                </table>

              </div>
            </div>
          </div>

        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    @else
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card  ">
            @if(Session::has('success'))
            <div class="alert alert-success">
              <strong>Success!</strong> {{Session::get('success')}}
            </div>
            @elseif(Session::has('failed'))
            <div class="alert alert-success">
              <strong>Failed!</strong> {{Session::get('failed')}}
            </div>
            @endif
            <div class="card-body">

              <form method="post" action=" {{ route('oemAdmin.categoryUpdate') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="small mb-1" for="inputLastName">Category</label>
                      <input type="text"  value="{{ $getCategory[0]['category_id'] }}" name="category_id">
                      <input class="form-control" name="category" type="text" value="{{ $getCategory[0]['category'] }}" placeholder="Enter Category">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="small mb-1" for="inputLastName">Category Logo</label>
                      <br/>
                      <input  name="logoUpdate" type="file" value="{{ $getCategory[0]['logo'] }}">
                    </div>
                    <img src="{{ asset('img/category/'.$getCategory[0]['logo'])}}" class="avatar" style="width: 80px;">
                  </div> 
                </div>

                <div class="form-row">
                  <div class="col-md-2">
                    <input type="submit" class="btn btn-primary btn-block" name="submit" value="Create"> 
                  </div>
                </div>
                <div class="form-group mt-4 mb-0"></div>
              </form>
            </div>
          </div>
        </div>

      </div>
    </div>
    @endif
  </section>
  <!-- /.content -->
</div>
@endsection

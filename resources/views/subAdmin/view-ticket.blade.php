 @extends('layouts.app')
 

@section('content')
 <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-sm-6">
          <h2 class="m-0"> View Tickets</h2>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
  <section class="content">
      <div class="container-fluid">
        
          <div class="row">
              <div class="col-lg-12  ">
                <div class="card">
                   <!-- <div class="card-header text-right"></div> -->
                  <div class="card-body">
                     <h4 class="card-title text-right">
                       <button class="btn" style="border: 1px solid #2c349c;"  >
                        <a href="#"  style="color:#2c349c !important;" onclick="markAllTicket()">
                            Mark all as read
                        </a>
                       </button>
                     </h4>
                    @if(count($ticketDetails))
                       @foreach ($ticketDetails as $view)  
                       <blockquote class="blockquote blockquote-primary" style="border: 1px solid #2c349c;"  id="row_{{  $view['ticket_id'] }}">
                        <p><b> Service Type:-</b>{{ $view['service_type'] }}.</p>
                         <p><b>Message:-</b> {{ $view['description'] }}.</p>
                         <footer class="blockquote-footer"> <a class="mark-as-read" href="#" onclick="sendMarkTicket('{{  $view['ticket_id'] }}')" style="color: #b66dff !important">Mark As read</a>
                         </footer>
                      </blockquote>
                       @endforeach
                    @else
                    <blockquote class="blockquote blockquote-primary"  >
                      <p><b> Ticket Request Not Available.</p>
                       
                       
                    </blockquote>
                    @endif
                  </div>
                </div>
                
              </div>
            </div>
         
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
 
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\Products;
use App\UserProducts;
use Illuminate\Support\Facades\DB;
class TicketController extends Controller
{
    

     public function index($slug) {
       
      $product  = Products::where('slug', $slug)->first()->toArray();
      $data['userProductDetails'] = UserProducts::join('tbl_products as p', 'p.product_id', '=', 'tbl_user_products.product_id')
            ->join('tbl_category as cat', 'cat.category_id', '=', 'tbl_user_products.category_id')
            ->where('p.slug', '=', $slug)
            ->where('tbl_user_products.product_id', '=', $product['product_id'])
              ->first()->toArray();
      return view('ticket.create',$data);
     }
     

     public function postTicket(Request $request) {
          
         $request->validate([
            'service_type' => 'required',
            'description' => 'required',
        ]);
           
           $dataArray       =    array(
            "admin"         =>     'admin',
            "oem_cust_id"   =>      session('Customer_logged')['cust_id'],
            "service_type"  =>      $request->service_type, 
            "description"   =>      $request->description 
           );

            $ticket  = Ticket::create($dataArray);
            if($ticket) {

		         return redirect('users/dashboard')->with('success', 'Send Renew Request Successfully');

	            } else {

	              return back()->with("failed", "Failed to Send Renew Request");
	            }
           
          // return back()->with("failed", 'Oppes! You have entered invalid credentials');
 		
     }

     public function markTicket(Request $request){
      
      return Db::table('tickets')->where('ticket_id', $request->ticket_id)->update(['read_at' => now()]);

     }
      public function markAllTicket(Request $request){
      return Db::table('tickets')->update(['read_at' => now()]);

     }

       public function viewTicket() {
             return view('view-ticket');
        }
       public function Adminticket() {
            
         $data['ticketDetails'] = Ticket::join('customer as c', 'c.cust_id', '=', 'tickets.oem_cust_id')
             ->get()->toArray();
        return view('admin.view-ticket', $data);
     
       }


       public function Oemticket() {
               $data['ticketDetails'] = Ticket::whereNull('read_at')->join('customer as c', 'c.cust_id', '=', 'tickets.oem_cust_id')
             ->get()->toArray();
             return view('subAdmin.view-ticket', $data);
     
       }  
}

<?php

namespace App\Http\Controllers;

use App\Notifications\NotificationCreated;
use Illuminate\Http\Request;
use App\Notifications\NewPostNotify;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Auth;
use App\User;
use App\Category;
use Illuminate\Support\Facades\DB;
class Notification extends Controller
{ 
     public function sendServiceNotification(Request $request)
      {    

      	 $users = User::first();
      	    $user = [
      	     'user_id' =>  Auth::user()->id,
      	     'name'   => 'Sonu Mishra',
      	     'email' =>  'ss@gmail.com',	
      	     ];
         $users->notify(new NotificationCreated($user));
          notify()->preset('user-updated');
      	     //  notify()->preset('user-updated');
           session::flash('success','Notification Send Successfully');
            return redirect()->back();
      }

 public function markNotification(Request $request)
    {
       
        $update= ['read_at' =>now(), ];
         DB::table('notifications')->where('id', $request->input('id'))->update($update);
        return response('200');
       
   }

  public function markAllNotification(Request $request) { 
     
      $update= ['read_at' =>now(), ];
         DB::table('notifications')->update($update);
         return response('200');
  }



   public function displayNotification(){
  	    
       $user = User::find(1);
        $data['notifications'] = $user->unreadNotifications;
           return view('notification',$data);
 
  }

// Send Notification to User

  public function  sendUserNotification(Request $request){
      
      if ($request->isMethod('post')) { 
          $request->validate([
              'manufacturer' => 'required',
              'description' => 'required',
          ]);

            $users = User::first();
              $user = [
               'send_by' =>1,
               'manufacturer' => $request->manufacturer,
               'message' =>  $request->description,  
               ];
         $users->notify(new NotificationCreated($user));
        return back()->with("success", "Notification Send Successfully");
          
       } else {
      
        $data['Categories'] = Category::get();
        return view('userNotification', $data); 
           
      }    
  }

}  
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{   
	protected $table= "customer";

     protected $fillable = [
        'name', 'email','mobile','otp_verify','activate','is_customer','password','profile','city','state','pincode',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
 


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
   
   
}

<?php

namespace App;

class SendOTP  
{
      public static function SendCode($phone){
        $code =rand(1111,9999);
        $nexmo=app('Nexmo\Client');
        $nexmo->message()->send([
                'to'   => '+91'.(int) $phone,
                'from' => 'Ghanshyam',
                'text' => 'Verify Code:'.$code
            ]);
        return $code;
      }     
 
}
